defmodule Waker.BidFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Waker.Bid` context.
  """

  @doc """
  Generate a offer.
  """
  def offer_fixture(attrs \\ %{}) do
    {:ok, offer} =
      attrs
      |> Enum.into(%{
        link: "http://allegro.pl/oferta/item-101",
        price: 120.5,
        title: "some title",
        type: :allegro
      })
      |> Waker.Bid.create_offer()

    offer
  end
end
