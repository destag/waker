defmodule Waker.Bid.OfferLink do
  use Ecto.Schema
  import Ecto.Changeset

  embedded_schema do
    field :link, EctoFields.URL
  end

  @doc false
  def changeset(offer_link, attrs) do
    offer_link
    |> cast(attrs, [:link])
    |> validate_required([:link])
  end

  def validate(changeset) do
    changeset
    |> apply_action(:validate)
  end
end
