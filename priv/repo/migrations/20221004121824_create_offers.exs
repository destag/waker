defmodule Waker.Repo.Migrations.CreateOffers do
  use Ecto.Migration

  def change do
    create table(:offers) do
      add :title, :string
      add :link, :string
      add :price, :float
      add :type, :string

      timestamps()
    end
  end
end
